import { Injectable,Component,OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import swal from 'sweetalert2';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
// import { UploadService } from '../services/upload.service';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic.component.html',
  styleUrls: ['./dynamic.component.css']
})

@Injectable()
export class DynamicComponent implements OnInit {
  SuccessResp=null;
  response=null;      
  id=0;
  title=null;
  error=null;
  PageNo=0;
  selectedFiles;

  constructor(private http:HttpClient)
  { 
 
    // this.http.get("https://dt-rebatepromotions.s3.us-east-2.amazonaws.com/config.json").subscribe(data =>{
    //   console.log("son : " + data);
    //   alert(JSON.stringify(data));
    // })
  }

  ngOnInit() {
      this.SuccessResp = 
    {
      serviceUrl: 'submission/create',
      pages: [
        {
            pageId: 'home',
                serviceUrl: 'offer',
                title: 'Home',
                fields: [
                    {
                        name: 'logo',
                        type: 'img',
                        src: 'assets/1536798901299.png'
                    },
                    {
                      name: 'header',
                      fields: [
                        {
                            name: 'submit',
                            title: 'Submit',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/home'
                        },
                        {
                            name: 'track',
                            title: 'Track',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/tracker'
                        },
                        {
                            name: 'faq',
                            title: 'FAQ',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/faqs'
                        },
                        {
                            name: 'contact',
                            title: 'Contact Us',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/contact'
                        }
                    ]
                },
                {
                    name: 'banner',
                    type: 'img',
                    src: 'assets/banner.png'
                },
                {
                  name: 'footer',
                  title: '© 2020 Reinalt Thomas',
                  fields: [
                        {
                            name: 'legal',
                            title: 'Legal',
                            type: 'link',
                            href: '#'
                        },
                        {
                            name: 'privacy',
                            title: 'Privacy Policy',
                            type: 'link',
                            href: '#'
                        },
                        {
                            name: 'faq',
                            title: 'FAQ',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/faqs'
                        },
                        {
                            name: 'contact',
                            title: 'Contact Us',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/contact'
                        }
                    ]
                },
                {
                  name: 'submission',
                  serviceUrl: 'validateSubmission',
                  fields: [
                        {
                            name: 'txtSubmit',
                            title: "Welcome to the America's Tire Rebate Center. Enter your offer code and purchase date to begin your submission.<br /><br />Click <a href='https://www.americastire.com/promotions' target='_blank'>here</a> to find current rebate offers.",
                            type: 'content'
                        },
                        {
                            name: 'lblSubmit',
                            title: "Enter offer and invoice date.",
                            type: 'heading'
                        },
                        {
                            name: 'offer',
                            title: 'Offer', 
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'invoiceDate',
                            title: 'Invoice Date',
                            type: 'date',
                            required: true
                        },
                        {
                            name: 'btnSubmit',
                            title: "Continue >",
                            type: 'button'
                        }
                    ]
                },
                ],
                onSuccess: 'invoiceInfo',
                onError: 'invoiceInfo',
            },
        {
            pageId: 'customerInfo',
            title: 'Customer Information',
            fields: [
                {
                    name: 'lblCust',
                    title: "Customer Information",
                    type: 'heading'
                },
                {
                    name: 'customerFName',
                    title: 'First Name',
                    type: 'string',
                    minLength: 3,
                    maxLength: 20,
                    required: true,
                },
                {
                    name: 'customerLName',
                    title: 'Last Name',
                    type: 'string',
                    minLength: 3,
                    maxLength: 20,
                    required: true,
                },
                {
                    serviceUrl: 'address/validate',
                    fields: [
                        {
                            name: 'customerAddress1',
                            title: 'Address 1',
                            type: 'string',
                            minLength: 5,
                            maxLength: 26,
                            required: true,
                        },
                        {
                            name: 'customerAddress2',
                            title: 'Address 2',
                            type: 'string',
                            minLength: 5,
                            maxLength: 26,
                            required: false,
                        },
                        {
                            name: 'customerCity',
                            title: 'City',
                            type: 'string',
                            minLength: 5,
                            maxLength: 20,
                            required: true,
                            options: [
                               {name: "Aberdeen",value: "Aberdeen"}, 
                               {name: "Abilene",value: "Abilene"}, {name: "Akron",value: "Akron"}
                                ]
                        },
                        {
                            name: 'customerState',
                            title: 'State',
                            type: 'string',
                            minLength: 5,
                            maxLength: 26,
                            options: [
                                {
                                    name: 'Alaska',
                                    value: 'AK',
                                },
                                {
                                    name: 'Arizona',
                                    value: 'AZ',
                                }
                            ],
                            required: true,
                        },
                        {
                            name: 'zip',
                            title: 'Zip',
                            type: 'string',
                            minLength: 5,
                            maxLength: 10,
                            required: true,
                        }, 
                        //Below fields added for current customer form testing
                        {
                            name: 'retailerId',
                            type: 'string',
                            required: true,
                        },
                        {
                            name: 'invoiceId',
                            type: 'string',
                            required: true,
                        },
                        // {
                        //     name: 'processDate',
                        //     type: 'date',
                        //     required: true,
                        // },
                        {
                            name: 'email',
                            type: 'email',
                            required: true,
                        },
                        {
                            name: 'phone',
                            type: 'string',
                            required: true,
                        }
                        // ,
                        // {
                        //     name: 'offerId',
                        //     type: 'number',
                        //     required: true,
                        // }
                        ,{
                            name: 'btnSubmit',
                            title: "Submit",
                            type: 'button'
                        }

                    ]
                }
            ]
            ,
            onSuccess: 'invoiceInfo',
            onError: 'invoiceInfo'
        },
{
            pageId: 'invoiceInfo',
            title: 'Invoice Information',
            fields: [
                {
                    serviceUrl: 'invoice/upload',
                    fields: [
                    {
                        name: 'lblUpload',
                        title: "Upload Invoice",
                        type: 'heading'
                    },
                    {
                            name: 'invoiceImg1',
                            type: 'file',
                            size: 3.5,
                            title: 'Invoice Image 1',
                            required: true,
                        },
                        {
                            name: 'invoiceImg2',
                            type: 'file',
                            size: 3.5,
                            title: 'Invoice Image 2',
                            required: false,
                        }
                        ,
                        {
                            name: 'btnSubmit',
                            title: "Submit",
                            type: 'button'
                        }

                    ] 
                }
            ]
        }        
      
      ]
    };

    this.response = 
    {
        pages: [
            {
                         serviceUrl: 'offer',
                title: 'Home', 
                fields: [
                    {
                        name: 'logo',
                        type: 'img',
                        src: 'assets/1536798901299.png'
                    },
                    {
                      name: 'header',
                      fields: [
                        {
                            name: 'submit',
                            title: 'Submit',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/home'
                        },
                        {
                            name: 'track',
                            title: 'Track',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/tracker'
                        },
                        {
                            name: 'faq',
                            title: 'FAQ',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/faqs'
                        },
                        {
                            name: 'contact',
                            title: 'Contact Us',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/contact'
                        }
                    ]
                },
                {
                    name: 'banner',
                    type: 'img',
                    src: 'assets/banner.png'
                },
                {
                  name: 'footer',
                  title: '© 2020 Reinalt Thomas',
                  fields: [
                        {
                            name: 'legal',
                            title: 'Legal',
                            type: 'link',
                            href: '#'
                        },
                        {
                            name: 'privacy',
                            title: 'Privacy Policy',
                            type: 'link',
                            href: '#'
                        },
                        {
                            name: 'faq',
                            title: 'FAQ',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/faqs'
                        },
                        {
                            name: 'contact',
                            title: 'Contact Us',
                            type: 'link',
                            href: 'https://at.rebatepromotions.com/#/contact'
                        }
                    ]
                },
                {
                  name: 'submission',
                  serviceUrl: 'validateSubmission',
                  fields: [
                        {
                            name: 'txtSubmit',
                            title: "Welcome to the America's Tire Rebate Center. Enter your offer code and purchase date to begin your submission.<br /><br />Click <a href='https://www.americastire.com/promotions' target='_blank'>here</a> to find current rebate offers.",
                            type: 'content'
                        },
                        {
                            name: 'lblSubmit',
                            title: "Enter offer and invoice date.",
                            type: 'heading'
                        },
                        {
                            name: 'offer',
                            title: 'Offer', 
                            type: 'string',
                            required: true
                        },
                        {
                            name: 'invoiceDate',
                            title: 'Invoice Date',
                            type: 'date',
                            required: true
                        },
                        {
                            name: 'btnSubmit',
                            title: "Continue >",
                            type: 'button'
                        }
                    ]
                },
                ],
                onSuccess: 'customerInfo',
                onError: 'customerInfo',
            }
        ]
    };

    this.createForm();
  }

uploadFile(file,dest_file) {
    const contentType = file.type;
    const bucket = new S3(
          {
              signatureVersion: 'v4',
          accessKeyId: 'AKIAZWDAJRFR4DLI7BXA',
          secretAccessKey: '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',
          region: 'us-east-2'
          }
      );
      
      const params = {
          Bucket: 'posdatadump',
          Key: dest_file,
          Body: file,
          ACL: 'public-read',
          ContentType: contentType
      };
      bucket.upload(params, function (err, data) {
          if (err) {
              console.log('There was an error uploading your file: ', err);
              alert('There was an error uploading your file: ' + JSON.stringify(data));
              return false;
          }
          console.log('Successfully uploaded file.', data);
          alert('Successfully uploaded file! ' + JSON.stringify(data));
          return true;
      });
}

submitFrm()
{ 
    var formElement = <HTMLFormElement>document.getElementById("submitForm");
    var formData = new FormData(formElement);
    var onSuccess = {pages: [] };
    var onError = {pages: [] };
  
    if(this.response.pages.length > 0)
    {
        for(var i=1;i<this.SuccessResp.pages.length;i++)
        {
            if(this.response.pages[0].hasOwnProperty("onSuccess"))
            {

              if(this.SuccessResp.pages[i].pageId == this.response.pages[0].onSuccess)
              {
                onSuccess.pages.push(this.SuccessResp.pages[i]);
                this.PageNo = i - 1;
              }
            }
            if(this.response.pages[0].hasOwnProperty("onError"))
            {

              if(this.SuccessResp.pages[i].pageId == this.response.pages[0].onError)
              {
                this.PageNo = i - 1;
                onError.pages.push(this.SuccessResp.pages[i]);
              }
            }
        }
    }
    // let headers = new HttpHeaders({'Content-Type': 'application/json','Authorization': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv','X-Api-Key': 'ijls4hqzTq57CgTojBf5s40NrkWY320h10u9YVYv'});
    let headers = new HttpHeaders({ 'access-control-allow-origin': '*','Content-Type': 'application/json'});
    let options = {headers: headers};
    var body="{";
    var field="";
    var value="";
    var type="";
    for(var i=0;i<this.response.pages[0].fields.length; i++)
    {
      if(this.response.pages[0].fields[i].fields)
      {
        if(this.response.pages[0].fields[i].fields.length > 0)
        {
          for(var j=0;j<this.response.pages[0].fields[i].fields.length;j++)
          {
            type = this.response.pages[0].fields[i].fields[j].type;
            if(type == "string" || type=="date" || type=="file" || type=="email")
            {
              field = this.response.pages[0].fields[i].fields[j].name;
              value = this.getFieldsValue(field,type);

              body += "\"" + field + "\":\"" + value + "\",";
            }
          }
        }
      }
      else
      {
        type = this.response.pages[0].fields[i].type;

        if(type == "string" || type=="date" || type=="file" || type=="email")
        {
          field = this.response.pages[0].fields[i].name;
          value = this.getFieldsValue(field,type);
                 
          body += "\"" + field + "\":\"" + value + "\",";
        }
      }
    }
    body = body.substring(0,body.length-1);
    body += "}";

    // let result = this.http.post('https://isff5c4lif.execute-api.us-east-2.amazonaws.com/staging/validateSubmission', body,options);
    // let result = this.http.post('https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate', body,options);
    
    // result.subscribe(
    //     res => {
    //       console.log(res);

    //       var data = JSON.stringify(res);
                
    //             this.response = onSuccess;
    //             // alert("PageNo : " + this.PageNo);
    //             // alert("onSuccess : " + JSON.stringify(onSuccess));
    //           if(onSuccess.hasOwnProperty("pages"))
    //           {
    //             if(onSuccess.pages.length > 0)
    //             {
    //                 this.response= onSuccess;
    //                 this.id=0;
    //                 this.createForm();
    //             }
    //             else
    //             {
    //                 var resData="Submitted Successfully!";
    //                 if(data.hasOwnProperty("data"))
    //                 {
    //                   if(data["data"].length > 0)
    //                   {
    //                     resData = data["data"][0];
    //                   }
    //                   else
    //                   {
    //                     resData = data["data"];
    //                   }
    //                 }
    //                 else
    //                 {
    //                   resData = data;
    //                 }
    //                 this.title=resData;
    //                 alert(this.title);
    //                 // this.response = null;
    //                 // document.getElementById("btn").innerHTML = '';
    //              }
    //           }
    //           // this.createForm();
    //     },
    //     err => {
    //       console.log("Error occured");
    //       // alert(JSON.stringify(err));

    //        this.response = onError;
           
    //       if(onError.pages.length > 0)
    //       {
    //           this.response = onError;
    //           this.id=0;
    //           this.createForm();
    //       }
    //       else
    //       {
    //           var resData = "Try Again Later!";
    //           // this.id=this.PageNo;
    //           alert(resData);
    //       }
    //     }
    //   );
        let result = this.http.post('https://8p1q4zs910.execute-api.us-east-2.amazonaws.com/staging/offervalidate', body, options);
    result.subscribe(
      res => {
        console.log(res);
        var data = JSON.stringify(res);
        //console.log(JSON.parse(res['body']));
        if( res['body'] == "Successful Validation"){
          this.response = onSuccess;
          if (onSuccess.hasOwnProperty("pages")) {
            if (onSuccess.pages.length > 0) {
              this.response = onSuccess;
              this.id = 0;
              this.createForm();
            }
            else {
              var resData = "Submitted Successfully!";
              if (data.hasOwnProperty("data")) {
                if (data["data"].length > 0) {
                  resData = data["data"][0];
                }
                else {
                  resData = data["data"];
                }
              }
              else {
                resData = data;
              }
              this.title = resData;
              alert(this.title);
              // this.response = null;
              // document.getElementById("btn").innerHTML = '';
            }
          }
        }
        else if( res['body'] == 'Offer Has closed for Submissions'){
          alert('Entered OfferId does not exist.Please Check information.');
        }
        // alert("PageNo : " + this.PageNo);
        // alert("onSuccess : " + JSON.stringify(onSuccess));
        
        // this.createForm();
      },
      err => {
        console.log("Error occured");
        // alert(JSON.stringify(err));

        this.response = onError;

        if (onError.pages.length > 0) {
          this.response = onError;
          this.id = 0;
          this.createForm();
        }
        else {
          var resData = "Try Again Later!";
          // this.id=this.PageNo;
          alert(resData);
        }
      }
    );
  }

  getFieldsValue(field,type="")
  {
      var value="";
      if(type == "file")
      {
          // var file = <HTMLInputElement>document.forms["submitForm"].elements[field];
    
          // if(file.files.length == 0)
          // {
          //   return "";
          // }

          // var src_file = file.files[0];
          // var ext=src_file["name"].substring(src_file["name"].lastIndexOf('.')+1, src_file["name"].length);
          //   //       
          //   var dest_file = 'dynamic_uploads/' + new Date().getTime() + "." + ext;
          //   value = "https://s3.console.aws.amazon.com/s3/buckets/posdatadump/" + dest_file;
              
          //       // this.uploadFile(file,dest_file);
          //   const contentType = file.type;
          //   const bucket = new S3(
          //         {
          //             signatureVersion: 'v4',
          //         accessKeyId: 'AKIAZWDAJRFR4DLI7BXA',
          //         secretAccessKey: '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',
          //         region: 'us-east-2'
          //         }
          //     );
              
          //     const params = {
          //         Bucket: 'posdatadump',
          //         Key: dest_file,
          //         Body: src_file,
          //         ACL: 'public-read',
          //         ContentType: contentType
          //     };
          //     bucket.upload(params, function (err, data) {
          //         if (err) {
          //             console.log('There was an error uploading your file: ', err);
          //             // alert('There was an error uploading your file: ' + JSON.stringify(data));
          //             return false;
          //         }
          //         console.log('Successfully uploaded file.', data);
          //         // alert('Successfully uploaded file! ' + JSON.stringify(data));
          //         // return value;
          //     });
          
var file = <HTMLInputElement>document.forms["submitForm"].elements[field];


        if(file.files.length == 0)
        {
          return "";
        }


        var src_file = file.files[0];
        var ext=src_file["name"].substring(src_file["name"].lastIndexOf('.')+1, src_file["name"].length);
        var offerCode = 1006;
        let invoiceId = 'invoiceId';
        let lastName = 'lastName';
          //
          var dest_file = offerCode+'/' + lastName+'_'+invoiceId + "." + ext;
          value = "https://s3.console.aws.amazon.com/s3/buckets/customerinvoices/" + dest_file;


              // this.uploadFile(file,dest_file);
          const contentType = file.type;
          const bucket = new S3(
                {
                    signatureVersion: 'v4',
                accessKeyId: 'AKIAZWDAJRFR4DLI7BXA',
                secretAccessKey: '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',
                region: 'us-east-2'
                }
            );


            const params = {
                Bucket: 'customerinvoices',
                Key: dest_file,
                Body: src_file,
                ACL: 'public-read',
                ContentType: contentType
            };
            bucket.upload(params, function (err, data) {
                if (err) {
                    console.log('There was an error uploading your file: ', err);
                    // alert('There was an error uploading your file: ' + JSON.stringify(data));
                    return false;
                }
                console.log('Successfully uploaded file.', data);
                // alert('Successfully uploaded file! ' + JSON.stringify(data));
                // return value;
            });
      }               
      else
      {
        var val = <HTMLInputElement>document.forms["submitForm"].elements[field];
        value = val.value;
      }
    return value;
  }
  createForm()
  {
    document.getElementById("myForm").innerHTML = '';
  
    if(this.title != null)
    {
      var str = "<tbody>";
      var arr = this.title.split(',');
      var name = "";
      var value = "";
      var keyVal;
    
      for(var i=0;i<arr.length;i++)
      {
        keyVal = arr[i].split(":");
        name = keyVal[0];
        value = keyVal[1];
        if(i==0)
          name = name.substr(2,name.length);
        else if(i==arr.length - 1)
          value = value.substr(0,value.length - 2);
        str = str + '<tr style="color:green;""><td style="text-align:left;">'+name+'</td><td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;'+value+'</td></tr>';
    
      }
      // $("#tbl").append('</tbody>');
      str = str + '</tbody>';
      if(keyVal.length > 1)
        document.getElementById("tbl").innerHTML = str;
      else
        document.getElementById("msg").innerHTML = this.title;
        
      // var btnBk = document.createElement("button");
      // btnBk.innerHTML = "Back";
      // btnBk.setAttribute("class","btnRegister");
      // btnBk.setAttribute("onclick","window.history.back();");
       
      //  document.getElementById("btnBack").appendChild(btnBk);
    }
    else if(this.error != null)
    {
      alert(this.error);
      // this.sweetAlert(this.error);
      // var h = document.createElement("div");
     //    h.setAttribute("style","color:red");
     //    h.innerHTML = error;
  
    //     document.getElementById("msg").appendChild(h);
    
    }
    var id = this.id;
    // var h = this.response.pages[id].title;
    // document.getElementById("pageTitle").innerHTML = h;
    for(var i=0;i<this.response.pages[id].fields.length;i++)
    {
      if(this.response.pages[id].fields[i].fields)
      {

        if(this.response.pages[id].fields[i].fields.length > 0)
        {
            for(var j=0;j<this.response.pages[id].fields[i].fields.length;j++)
            {
              this.createField(this.response.pages[id].fields[i].fields[j].name,this.response.pages[id].fields[i].fields[j].title,this.response.pages[id].fields[i].fields[j].type,this.response.pages[id].fields[i].fields[j].minLength,this.response.pages[id].fields[i].fields[j].maxLength,this.response.pages[id].fields[i].fields[j].required,this.response.pages[id].fields[i].fields[j].name,this.response.pages[id].fields[i].fields[j].options,this.response.pages[id].fields[i].fields[j].size,this.response.pages[id].fields[i].fields[j].src,this.response.pages[id].fields[i].fields[j].href,this.response.pages[id].fields[i].name);
            }
        }
      }
      else
      {
        this.createField(this.response.pages[id].fields[i].name,this.response.pages[id].fields[i].title,this.response.pages[id].fields[i].type,this.response.pages[id].fields[i].minLength,this.response.pages[id].fields[i].maxLength,this.response.pages[id].fields[i].required,this.response.pages[id].fields[i].name,this.response.pages[id].fields[i].options,this.response.pages[id].fields[i].size,this.response.pages[id].fields[i].src,this.response.pages[id].fields[i].href);
      }
    }
  }

  createField(name,title=null,type='string',minLength,maxLength,required,placeholder,options=null,size=1,src,href,headerFooter="")
  {
    var div = document.createElement("div");
    div.setAttribute("id", "divGrp" + name);
    div.setAttribute("class","col-md-12");
    document.getElementById("myForm").appendChild(div);

    var div1 = document.createElement("div");
    div1.setAttribute("id", "div" + name);
    div1.setAttribute("class","form-group");
    document.getElementById("divGrp" + name).appendChild(div1);

    if(type != "img" && type != "link" && type != "heading" && type != "content" && type != "button")
    {
      var h = document.createElement("h6");
      // h.innerHTML = "Customer First Name* :";
      if(title == null)
      {
        title = name;
      }
      if(required == true)
      {
        h.innerHTML = title + "* :";
      }
      else
      {
        h.innerHTML = title + " :";
      }
      document.getElementById("div" + name).appendChild(h);
    }
    if(options)
    {
      // var y = document.createElement("select");
  
      // y.setAttribute("id", name);
      // y.setAttribute("name", name);
     
      // y.setAttribute("type", "select");
     
      var x = document.createElement("SELECT");
      x.setAttribute("class","form-control");
      x.setAttribute("id", name);
      // document.body.appendChild(x);
      document.getElementById("div" +name).appendChild(x);
      
      for(var cnt=0;cnt<options.length;cnt++)
      {
        var z = document.createElement("option");
        z.setAttribute("value", options[cnt].value);
        
        var t = document.createTextNode(options[cnt].name);
        
        z.appendChild(t);
    
        document.getElementById(name).appendChild(z);
      }
      
      // var tyhead = '<input class="form-control type_ahead' + name + ' tt-query" type="text" name=' + name + ' autocomplete="off" spellcheck="false" Placeholder=' + title + ' minLength=' + minLength + ' maxLength=' + maxLength + ' required=' + required + '>';
      // // $("#divGrp" + name).append(tyhead);
      // document.getElementById("divGrp" + name).innerHTML=  document.getElementById("divGrp" + name).innerHTML + tyhead + "<br />";
    
      // let data = "<script type='text/javascript'>var options=" + JSON.stringify(options) + ";";
      // data = data + "var optionList = new Bloodhound({datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),queryTokenizer: Bloodhound.tokenizers.whitespace,local: options});";
      // data = data + "optionList.initialize();";
      // data = data + "$('.type_ahead" + name + "').typeahead(null, {name: 'optionList', displayKey: 'name', source: optionList.ttAdapter(), limit: 10});";
      // data = data + "</script>";

      // document.getElementById("divGrp" + name).innerHTML=  document.getElementById("divGrp" + name).innerHTML + data;
    
      return;
    }

    var y;
    if(type == "string" || type=="email")
    {
      y = document.createElement("INPUT");

      if(type=="email")
        y.setAttribute("type", type);
      else  
        y.setAttribute("type", "text");
      if(placeholder)
        y.setAttribute("Placeholder", placeholder);
      else
        y.setAttribute("Placeholder", title);

      y.setAttribute("id", name);
      y.setAttribute("name", name);
      y.setAttribute("class","form-control");
      if(minLength)
        y.setAttribute("minLength",minLength);
      if(maxLength)
        y.setAttribute("maxLength",maxLength);
      if(required)
        y.setAttribute("required",required);
  
  
      document.getElementById("div" + name).appendChild(y);

    }
    else if(type == "content")
    {
      y = document.createElement("h5");
      y.setAttribute("style", "margin-top:-200px;");   
      
      y.innerHTML = title;
      
      document.getElementById("div" + name).appendChild(y);
    }
    else if(type == "heading")
    {
      y = document.createElement("h4");
      
      y.innerHTML = title;
      document.getElementById("div" + name).appendChild(y);
    }
    else if(type == 'link')
    {
        var ul = document.getElementById(headerFooter);
        var li = document.createElement("li");
        li.setAttribute("class","nav-item");
       
        y = document.createElement("a");
        y.innerHTML = title;
        
        y.setAttribute("id", name);   
        y.setAttribute("href", href);   
        y.setAttribute("data-toggle","tab");
        y.setAttribute("role","tab");
        y.setAttribute("aria-controls",href);
        if(name == "submit")
        {
          y.setAttribute("aria-selected","true");
          y.setAttribute("class","nav-link");
          y.setAttribute("style","color:silver");
        }
        else
        {
          y.setAttribute("aria-selected","false");
          y.setAttribute("class","nav-link"); 
          y.setAttribute("style","color:white");
        }
        li.appendChild(y);
        ul.appendChild(li);
    }
    else if(type == 'img')
    {
      y = document.createElement("img");
      y.setAttribute("src", src);   
      
      if(name="banner")
      {
        document.getElementById("banner").appendChild(y);
      }
      else
        document.getElementById("pageTitle").appendChild(y);
    }
    else if(type == "file")
    {
      y = document.createElement("INPUT");
      y.setAttribute("type", "file");
      y.setAttribute("id", name);   
      y.setAttribute("name", name);   
      
      if(placeholder)
        y.setAttribute("Placeholder", placeholder);
      else
        y.setAttribute("Placeholder", title);
  
      document.getElementById("div" + name).appendChild(y);
      // y.setAttribute("onchange",validateSize(name,1));
      var file = <HTMLInputElement>document.getElementById(name);
      file.onchange = function() {
        // if(file.files[0].length > 0)
        // {
          // this.selectedFiles.push(file);
          if(file.files[0].size > (size * 1024 * 1024)){
            alert("File must be less than " + size + " MB.");
            // swal({
            //   title: "File must be less than " + size + " MB.",
            //   showCloseButton: true,
            //   confirmButtonText: 'Ok',
            //   allowOutsideClick: false,
            //   allowEscapeKey: false,
            //   showCancelButton: false,
            // }).then(function(dismiss) {
            //   //
            // });
            // this.value = "";
            file.files[0] = null;
          // }
        }
      }
    }
    else if(type == "date")
    {
      y = document.createElement("INPUT");
      y.setAttribute("type", "date");
      y.setAttribute("class","form-control");
      y.setAttribute("id", name);   
      y.setAttribute("name", name);   
      
      if(placeholder)
        y.setAttribute("Placeholder", placeholder);

      document.getElementById("div" + name).appendChild(y);
    }
    else if(type == "button")
    {
      // y = document.createElement("button");
      // y.setAttribute("type", "submit");
      // y.setAttribute("class","btnRegister");
      // y.innerHTML = title;
      // y.setAttribute("onclick","submitFrm();");
      
      // document.getElementById("btn").innerHTML=title;
    }  

    // else
    // {
    //   y.setAttribute("type", type);
    //   if(placeholder)
    //     y.setAttribute("Placeholder", placeholder);

    //   document.getElementById("div" + name).appendChild(y);
    // }
  }

  // sweetAlert(title)
  // {
  //   swal({
  //             title: title,
  //             showCloseButton: true,
  //             confirmButtonText: 'Ok',
  //             allowOutsideClick: false,
  //             allowEscapeKey: false,
  //             showCancelButton: false,
  //           }).then(function(dismiss) {
  //             //
  //           });
  // }

}
